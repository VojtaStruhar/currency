//
//  Dimensions.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

enum Dimensions {
    /// Used in main screen, tableView currency preview cell height
    static let currencyCellHeight = CGFloat(72)
    /// Used on collection view cells in Add currency screen
    static let currencyCornerRadius = CGFloat(32)
    
    static let headerHeight = CGFloat(26)
    
    static let addCellHeight = CGFloat(200)
    /// Width and height of the flag icon in collectionView cell in 'Add' screen
    static let collectionFlagDimension = CGFloat(40)
}
