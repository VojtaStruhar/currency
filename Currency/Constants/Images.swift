//
//  ImageNames.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

struct Images {
    static let plusSymbol   = UIImage(named: "icAdd")
    static let noWifi       = UIImage(named: "icNoInternet")
    static let emptyState   = UIImage(named: "emptyStateLogo")
}
