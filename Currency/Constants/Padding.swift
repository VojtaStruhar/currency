//
//  Padding.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

enum Padding {
    /// 4
    static let tiny = CGFloat(4)
    /// 8
    static let normal = CGFloat(8)
    /// 16
    static let medium = CGFloat(16)
}
