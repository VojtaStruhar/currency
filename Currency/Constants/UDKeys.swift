//
//  UDKeys.swift
//  Currency
//
//  Created by Vojtěch Struhár on 21/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import Foundation

enum UDKeys {
    static let rememberedCurrencyCode = "rememberedCurrencyCode"
    static let rememberedAddedCodesArray = "rememberedAddedCodesArray"
}
