//
//  Colors.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

extension UIColor {
    /// Lighter gray
    static let backgroundLight = UIColor(named: "backgroundLight")
    /// Dark gray, default background color
    static let background = UIColor(named: "backgroundDark")
    /// Tint yellow
    static let tint = UIColor(named: "tint")
    
    static let textLight = UIColor.white
    static let textDark = UIColor.black
}
