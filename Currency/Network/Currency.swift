//
//  Currency.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import Foundation

class Currency: Decodable, Hashable {
    
    static func == (lhs: Currency, rhs: Currency) -> Bool {
        return lhs.code == rhs.code
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(symbol)
    }
    
    let symbol:         String
    let name:           String
    let symbol_native:  String
    let decimal_digits: Int
    let rounding:       Double
    let code:           String
    let name_plural:    String
    let flag:           String
    
    var ratioToEUR:     Double?
    var selected:       Bool?
    var added:          Bool?
}
