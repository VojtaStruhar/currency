//
//  FixerResponse.swift
//  CleevioCurrency
//
//  Created by Vojtěch Struhár on 03/05/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import Foundation

struct FixerResponse : Decodable {
    let success: Bool
    let timestamp: Int
    let base: String
    let date: String
    let rates: [String: Double]
}

