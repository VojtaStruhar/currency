//
//  CTError.swift
//  CleevioCurrency
//
//  Created by Vojtěch Struhár on 03/05/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import Foundation

enum CTError: String, Error {   // enum has to conform to Error type so it can be used in Result type
    // networking
    case invalidUrl         = "Invalid URL"
    case unableToComplete   = "Unable to complete your request. Please, check your internet connection."
    case invalidData        = "The data received from the server was invalid. Please try again."
}
