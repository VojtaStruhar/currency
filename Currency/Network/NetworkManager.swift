//
//  NetworkManager.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

class NetworkManager {
    
    private static let hostUrl  = "http://data.fixer.io/api/latest"
    private static let apiKey   = "?access_key=a493d149d3ab7165ec4a8fde861463f1"
    
    static let shared = NetworkManager()
    private let cache = NSCache<NSString, UIImage>()
    
    static func getCurrencyRates(completion: @escaping(Result<FixerResponse, CTError>) -> Void) {
        guard let url = URL(string: hostUrl + apiKey) else {
            completion(.failure(.invalidUrl))
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                completion(.failure(.unableToComplete))
                return
            }
            if let data = data {
                do {
                    let decoded = try JSONDecoder().decode(FixerResponse.self, from: data)
                    completion(.success(decoded))
                } catch {
                    completion(.failure(.invalidData))
                }
            }
        }
        task.resume()
        
    }
    
    func downloadImage(from urlString: String, completion: @escaping (UIImage?) -> Void) {
        let cacheKey = NSString(string: urlString)
        if let image = cache.object(forKey: cacheKey) {
            // if there is cached image, set it and return
            completion(image)
            return
        }
        
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            guard let self = self,
                error == nil,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let data = data,
                let image = UIImage(data: data) else {
                    completion(nil)
                    return
            }
            
            self.cache.setObject(image, forKey: cacheKey)
            DispatchQueue.main.async {
                completion(image)
            }
            
        }
        task.resume()
    }
}
