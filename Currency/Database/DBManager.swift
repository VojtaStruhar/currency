//
//  DBManager.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

class DBManager {
    
    var currencies: [Currency] = []
    var selectedCurrency: Currency?
    
    static let shared = DBManager()
    
    init() {
        if let path = Bundle.main.path(forResource: "currencies", ofType: "json") {
            do {
                let stringData = try String(contentsOfFile: path, encoding: .utf8)
                let decoded = try JSONDecoder().decode([Currency].self, from: stringData.data(using: .utf8)!)
                for currency in decoded {
                    self.insertNew(currency: currency)
                }
            } catch let e { print(e) }
        }
    }
    
    private func insertNew(currency: Currency) {
        currency.selected = false
        currency.added = false
        currencies.append(currency)
    }
    
    func getAddedCurrencies() -> [Currency] {
        return currencies.filter({ $0.added! })
    }
    
    func updateCurrencyValue(code: String, to value: Double) {
        for c in currencies where c.code == code {
            c.ratioToEUR = value
        }
    }
    
    func updateCurrencyToSelected(code: String) {
        for c in currencies {
            if c.code == code {
                c.selected = true
                selectedCurrency = c
                UserDefaults.standard.set(code, forKey: UDKeys.rememberedCurrencyCode)
            } else {
                c.selected = false
            }
        }
    }
    
    func updateCurrencyAdded(code: String) {
        for c in currencies where c.code == code {
            c.added = !c.added!
        }
        DispatchQueue.global(qos: .background).async {
            var rememberedAddedCodesArray: [String] = []
            for c in self.currencies where c.added! {
                rememberedAddedCodesArray.append(c.code)
            }
            UserDefaults.standard.set(rememberedAddedCodesArray, forKey: UDKeys.rememberedAddedCodesArray)
        }
    }
}
