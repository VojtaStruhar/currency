//
//  AddNavigationController.swift
//  Currency
//
//  Created by Vojtěch Struhár on 20/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

class AddNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let vc = AddViewController()
        vc.title = "Add currency"
        viewControllers = [vc]
        
    }
    

}
