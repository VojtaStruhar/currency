//
//  MainNavigationController.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc = MainViewController()
        vc.title = "Currency"
        
        viewControllers = [vc]
    }

}
