//
//  AddViewController.swift
//  Currency
//
//  Created by Vojtěch Struhár on 20/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {
    
    var filterring = false
    var filtered: [[Currency]] = []
    
    let cellIdentifier = "cellIdentifier"
    var dataSource: [[Currency]] = []
    var sectionTitles: [String] = []
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = .background
        tv.separatorStyle = .none
        tv.tintColor = .tint
        tv.sectionIndexBackgroundColor = .background
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareDataSource()
        setupUI()
        setupTableViewDelegate()
        setupSearchDelegate()
    }
    
    func prepareDataSource() {
        let currenciesAlphabetically = DBManager.shared.currencies.sorted { $0.name.lowercased() < $1.name.lowercased()}
        var currentLetter = currenciesAlphabetically[0].name.prefix(1) {
            didSet {
                sectionTitles.append(String(currentLetter))
            }
        }
        sectionTitles.append(String(currentLetter))
        
        var currentArray: [Currency] = []
        for c in currenciesAlphabetically {
            if c.name.prefix(1) == currentLetter {
                currentArray.append(c)
            } else {
                dataSource.append(currentArray)
                currentArray = []
                currentLetter = c.name.prefix(1)
                currentArray.append(c)
            }
        }
        tableView.reloadData()
    }
    
    @objc func goBack() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setupTableViewDelegate() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CollectionTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filterring ? filtered.count : dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CollectionTableViewCell else { return UITableViewCell() }
        cell.dataSource = filterring ? filtered[indexPath.section] : dataSource[indexPath.section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = .background
        let label = UILabel()
        label.textColor = .tint
        label.text = filterring ? "Search results" : dataSource[section][0].name.prefix(1).uppercased()
        header.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: header.topAnchor),
            label.leadingAnchor.constraint(equalTo: header.leadingAnchor, constant: Padding.medium),
            label.trailingAnchor.constraint(equalTo: header.trailingAnchor),
            label.bottomAnchor.constraint(equalTo: header.bottomAnchor)
        ])
        
        return header
    }
    
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]!{
        var sectionsPreview: [String] = []
        for currencies in dataSource {
            let firstLetter = currencies[0].name.prefix(1).uppercased()
            sectionsPreview.append(firstLetter)
        }
        return sectionsPreview as [AnyObject]
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionTitles
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Dimensions.headerHeight
    }
    
}

extension AddViewController: UISearchResultsUpdating {
    
    
    func setupSearchDelegate() {
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.tintColor = .tint
        self.navigationItem.searchController = search
        self.navigationItem.searchController?.isActive = true
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        self.filtered = []
        if let text = searchController.searchBar.text, !text.isEmpty {
            let array = DBManager.shared.currencies.filter({ (currency) -> Bool in
                if currency.name.lowercased().contains(text.lowercased()) || currency.code.lowercased().contains(text.lowercased()) {
                    return true
                } else { return false }
            })
            self.filterring = true
            self.filtered.append(array)
        }
        else {
            self.filterring = false
            self.filtered = []
        }
        self.tableView.reloadData()
    }

    
}
