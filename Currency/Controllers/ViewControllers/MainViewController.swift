//
//  MainViewController.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    fileprivate var filtered: [Currency] = []
    fileprivate var filtering = false
    
    private var dataSource: [Currency] = []
    private lazy var diffableDataSource = makeDataSource()
    enum Section {
        case main
    }
    
    private var isEmptyStatePresented = false
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = .background
        tv.separatorStyle = .none
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let empty = CTEmptyStateView(message: "No favorite currencies. Try adding some with the + icon!")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupTableView()
        setupSearchDelegate()
        
        getData()
    }
    
    func getData() {
        let manager = DBManager.shared
        NetworkManager.getCurrencyRates { [weak self] (result) in
            guard let self = self else { return }
            
            switch result {
            case .success(let fixer):
                let rates = fixer.rates
                for c in manager.currencies {
                    manager.updateCurrencyValue(code: c.code, to: rates[c.code] ?? 0.0)
                }
                if let remembered = UserDefaults.standard.string(forKey: UDKeys.rememberedCurrencyCode) {
                    manager.updateCurrencyToSelected(code: remembered)
                } else {
                    // Only first launch, select what comes with API as a base
                    manager.updateCurrencyToSelected(code: fixer.base)
                }
                
                if let array = UserDefaults.standard.array(forKey: UDKeys.rememberedAddedCodesArray) as? [String] {
                    for code in array {
                        manager.updateCurrencyAdded(code: code)
                    }
                }
                self.updateDataSource()
                
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { [weak self] (_) in
                    guard let self = self else { return }
                    self.getData()
                }))
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func makeDataSource() -> UITableViewDiffableDataSource<Section, Currency> {
        let reuseIdentifier = CurrencyTableViewCell.reuseID
        
        return UITableViewDiffableDataSource(
            tableView: tableView,
            cellProvider: {  tableView, indexPath, currency in
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: reuseIdentifier,
                    for: indexPath
                    ) as! CurrencyTableViewCell
                cell.set(currency: currency)
                return cell
        }
        )
    }
    
    func updateDiffable(with list: [Currency], animate: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Currency>()
        snapshot.appendSections([.main])
        snapshot.appendItems(list)
        
        DispatchQueue.main.async {
            self.diffableDataSource.apply(snapshot, animatingDifferences: animate)
        }
    }
    
    func updateDataSource() {
        dataSource = DBManager.shared.getAddedCurrencies()
        updateDiffable(with: dataSource)
        
        if dataSource.isEmpty {
            if isEmptyStatePresented == false {
                DispatchQueue.main.async { self.presentEmptyState(in: self.view.safeAreaLayoutGuide.layoutFrame) }
            }
        } else {
            DispatchQueue.main.async { self.dismissEmptyState() }
        }
    }
    
    func presentEmptyState(in rect: CGRect) {
        empty.frame = rect
        empty.alpha = 0
        view.addSubview(empty)
        empty.layoutIfNeeded()
        UIView.animate(withDuration: 0.7) {
            self.empty.alpha = 1
        }
        isEmptyStatePresented = true
    }
    
    func dismissEmptyState() {
        for v in view.subviews where v == empty {
            UIView.animate(withDuration: 0.3, animations: {
                v.alpha = 0
            }) { (_) in
                DispatchQueue.main.async { v.removeFromSuperview() }
                self.isEmptyStatePresented = false
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateDataSource()
    }
    
    @objc func goToAddCurrency() {
        let addVC = AddNavigationController()
        addVC.modalPresentationStyle = .fullScreen
        present(addVC, animated: true)
    }
    
}

extension MainViewController : UITableViewDelegate  {
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = diffableDataSource
        tableView.register(CurrencyTableViewCell.self, forCellReuseIdentifier: CurrencyTableViewCell.reuseID)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let tapped = diffableDataSource.itemIdentifier(for: indexPath) {
            var currentSnapshot = diffableDataSource.snapshot()
            DBManager.shared.updateCurrencyToSelected(code: tapped.code)
            currentSnapshot.reloadSections([.main])
            diffableDataSource.apply(currentSnapshot, animatingDifferences: false)
            
        }
    }
}

extension MainViewController: UISearchResultsUpdating {
    
    func setupSearchDelegate() {
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        self.navigationItem.searchController = search
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        let added = DBManager.shared.getAddedCurrencies()
        DispatchQueue.main.async { searchController.searchBar.searchTextField.layoutIfNeeded() }
        guard let filter = searchController.searchBar.text, !filter.isEmpty else {
            filtering = false
            filtered = []
            updateDiffable(with: added)
            return
        }
        filtering = true
        filtered = added.filter({ (currency) -> Bool in
            return currency.name.lowercased().contains(filter.lowercased()) || currency.code.lowercased().contains(filter.lowercased())
        })
        
        updateDiffable(with: filtered)
        
    }
    
}
