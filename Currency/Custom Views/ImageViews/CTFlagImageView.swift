//
//  CTFlagIconView.swift
//  CleevioCurrency
//
//  Created by Vojtěch Struhár on 03/05/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

class CTFlagImageView: UIImageView {

    let placeholderImage = Images.noWifi

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        contentMode         = .scaleAspectFill
        backgroundColor     = .tint
        clipsToBounds       = true
        image               = placeholderImage
        translatesAutoresizingMaskIntoConstraints = false
    }
}
