//
//  CollectionTableViewCell.swift
//  Currency
//
//  Created by Vojtěch Struhár on 20/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

class CollectionTableViewCell: UITableViewCell {

    var dataSource: [Currency] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    let cellIdentifier = "cellIdentifier"
    let backView = UIView()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.background
        cv.showsHorizontalScrollIndicator = false
        cv.contentInset = UIEdgeInsets(top: 0, left: Padding.normal, bottom: 0, right: Padding.normal)
        return cv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupUI() {
        contentView.backgroundColor = .background
        backView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(backView)
        NSLayoutConstraint.activate([
            backView.topAnchor.constraint(equalTo: contentView.topAnchor),
            backView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            backView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            backView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            backView.heightAnchor.constraint(equalToConstant: Dimensions.addCellHeight)
        ])
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        backView.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: backView.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: backView.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: backView.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: backView.bottomAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: Dimensions.addCellHeight)
        ])
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CurrencyCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
    }
}

extension CollectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? CurrencyCollectionViewCell else {
            return UICollectionViewCell()
        }
        let currency = dataSource[indexPath.row]
        cell.set(currency: currency)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let c = dataSource[indexPath.row]
        c.added = c.added! ? true : false
        DBManager.shared.updateCurrencyAdded(code: c.code)
        DispatchQueue.main.async {
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        return CGSize(width: height * 4/5, height: height)
    }
    
}
