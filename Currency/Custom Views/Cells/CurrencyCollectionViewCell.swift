//
//  CurrencyCollectionViewCell.swift
//  Currency
//
//  Created by Vojtěch Struhár on 20/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

class CurrencyCollectionViewCell: UICollectionViewCell {
    
    
    let backView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .backgroundLight
        view.layer.cornerRadius = Dimensions.currencyCornerRadius
        return view
    }()
    
    let flag: CTFlagImageView = CTFlagImageView(frame: .zero)
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 3
        return label
    }()
    
    let codeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func set(currency: Currency) {
        nameLabel.text = currency.name.uppercased()
        codeLabel.text = currency.code
        
        NetworkManager.shared.downloadImage(from: currency.flag) { [weak self] (image) in
            guard let self = self else { return }
            self.flag.image = image
        }
        
        if currency.added! {
            backView.backgroundColor = .tint
            nameLabel.textColor = .textDark
            codeLabel.textColor = .textDark
        } else {
            backView.backgroundColor = .backgroundLight
            nameLabel.textColor = .label
            codeLabel.textColor = .label
        }
    }
    
    func setupUI() {
        contentView.backgroundColor = .background
        
        backView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(backView)
        NSLayoutConstraint.activate([
            backView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Padding.tiny),
            backView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Padding.tiny),
            backView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Padding.tiny),
            backView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Padding.tiny)
        ])
        
        backView.addSubview(flag)
        flag.layer.cornerRadius = 20
        flag.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            flag.centerXAnchor.constraint(equalTo: backView.centerXAnchor),
            flag.topAnchor.constraint(equalTo: backView.topAnchor, constant: Padding.medium),
            flag.heightAnchor.constraint(equalToConstant: Dimensions.collectionFlagDimension),
            flag.widthAnchor.constraint(equalToConstant: Dimensions.collectionFlagDimension)
        ])
        
        let stack = UIStackView(arrangedSubviews: [nameLabel, codeLabel])
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.translatesAutoresizingMaskIntoConstraints = false
        backView.addSubview(stack)
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: flag.bottomAnchor, constant: Padding.normal),
            stack.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: Padding.normal),
            stack.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -Padding.normal),
            stack.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -Padding.normal)
        ])
        
    }
    
}
