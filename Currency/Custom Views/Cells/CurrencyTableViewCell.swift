//
//  CurrencyTableViewCell.swift
//  Currency
//
//  Created by Vojtěch Struhár on 19/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {
    
    static let reuseID = "CurrencyTableViewCell"
    
    let backView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.layer.cornerRadius = Dimensions.currencyCellHeight / 2
        return view
    }()
    
    let flag: CTFlagImageView = CTFlagImageView(frame: .zero)
    
    let currencyName = UILabel()
    
    let amountLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func set(currency: Currency) {
        let coeficient = Double(truncating: pow(10, currency.decimal_digits) as NSNumber)
        let selectedRatio = DBManager.shared.selectedCurrency?.ratioToEUR ?? 0
        let currencyRatio = currency.ratioToEUR ?? 1
        let roundedValue = round(currencyRatio / selectedRatio * coeficient) / coeficient
        amountLabel.text = "\(roundedValue) \(currency.symbol)"
        
        currencyName.text = currency.name
        NetworkManager.shared.downloadImage(from: currency.flag) { [weak self] (image) in
            guard let self = self else { return }
            self.flag.image = image
        }
        
        if currency.selected ?? false {
            backView.backgroundColor = .tint
            currencyName.textColor = .textDark
            amountLabel.textColor = .textDark
        } else {
            backView.backgroundColor = .backgroundLight
            currencyName.textColor = .label
            amountLabel.textColor = .label
        }
        
    }
    
    func setupUI() {
        contentView.backgroundColor = .background
        selectionStyle = .none
        
        backView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(backView)
        NSLayoutConstraint.activate([
            backView.topAnchor.constraint(equalTo: contentView.topAnchor),
            backView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Padding.normal),
            backView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Padding.normal),
            backView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Padding.normal),
            backView.heightAnchor.constraint(equalToConstant: Dimensions.currencyCellHeight)
        ])
        
        flag.layer.cornerRadius = Dimensions.currencyCellHeight / 2 - Padding.medium
        flag.translatesAutoresizingMaskIntoConstraints = false
        backView.addSubview(flag)
        NSLayoutConstraint.activate([
            flag.topAnchor.constraint(equalTo: backView.topAnchor, constant: Padding.medium),
            flag.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: Padding.medium),
            flag.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -Padding.medium),
            flag.widthAnchor.constraint(equalTo: flag.heightAnchor)
        ])
        
        amountLabel.translatesAutoresizingMaskIntoConstraints = false
        backView.addSubview(amountLabel)
        NSLayoutConstraint.activate([
            amountLabel.centerYAnchor.constraint(equalTo: backView.centerYAnchor),
            amountLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -Padding.medium)
        ])
        
        currencyName.translatesAutoresizingMaskIntoConstraints = false
        backView.addSubview(currencyName)
        NSLayoutConstraint.activate([
            currencyName.centerYAnchor.constraint(equalTo: backView.centerYAnchor),
            currencyName.leadingAnchor.constraint(equalTo: flag.trailingAnchor, constant: Padding.medium),
            currencyName.trailingAnchor.constraint(equalTo: amountLabel.leadingAnchor, constant: -Padding.normal)
        ])
        
    }

    
}
