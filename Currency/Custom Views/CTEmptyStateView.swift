//
//  CTEmptyStateView.swift
//  Currency
//
//  Created by Vojtěch Struhár on 04/05/2020.
//  Copyright © 2020 Vojta Struhár. All rights reserved.
//

import UIKit

class CTEmptyStateView: UIView {
    
    let messageLabel = CTTitleLabel(textAlignment: .center, fontSize: 30)
    let logoImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    convenience init(message: String) {
        self.init(frame: .zero)
        messageLabel.text = message
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        backgroundColor = .background
        
        addSubview(messageLabel)
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 60),
            messageLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Padding.medium),
            messageLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Padding.medium),
        ])
        
        addSubview(logoImageView)
        logoImageView.image = Images.emptyState
        logoImageView.alpha = 0.6
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        
//        logoImageView.frame = messageLabel.frame
        
        NSLayoutConstraint.activate([
            logoImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 60),
            logoImageView.widthAnchor.constraint(equalTo: self.widthAnchor),
            logoImageView.heightAnchor.constraint(equalTo: self.widthAnchor),
            logoImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 80)
        ])
    }
}
