//
//  AddVCLayout.swift
//  Currency
//
//  Created by Vojtěch Struhár on 20/09/2019.
//  Copyright © 2019 Vojta Struhár. All rights reserved.
//

import UIKit

extension AddViewController {
    func setupUI() {
        view.backgroundColor = .background
        
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.largeTitleDisplayMode = .never
        
        let btn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = btn
        
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

